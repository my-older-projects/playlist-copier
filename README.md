## Playlist Copier

This script copies music in a playlist into a folder. Its usage is simple, just give the location of the playlist file as first argument and it will copy all music under a folder with the same name of you playlist. If you want this script to copy into the current directory, then pass TRUE in second argument.

You can also require this.

This script currently onlu supports XSPF format, I will add more playlist format compatibilities sometime.