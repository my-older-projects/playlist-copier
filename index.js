'use strict';
const xml2js = require('xml2js');
const fs = require('fs-extra');
const path = require('path');

class Logger {
	constructor(on, trim) {
		this.on = on;
		this.trim = trim;
	}

	log(thing, level) {
		this.on && console.log(`${new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')} ${this.level || ''}: ${(this.trim && thing.length > this.trim * 2) ? `${thing.slice(0, this.trim)} ... ${thing.slice(-1 * this.trim)}` : thing}`);
	}
}

const procedure = (fullLoc, filename, folderpath, newFolFla) => {
	const file = new PlaylistFile(filename, fullLoc);
	const toFolder = new OutputFolder(filename.split('.')[0], folderpath, newFolFla);

	toFolder.create(() =>
		file.generateList(tlist =>
			tlist.copyAllTo(toFolder.toString(), () =>
				logger.log('Done.'))));
}

class Track {
	constructor(obj) {
		Object.keys(obj).forEach(key => this[key] = obj[key][0]);
		this.location = decodeURI(this.location.slice(7));
	}

	getLocation() {
		return this.location;
	}

	getTitle() {
		return this.title;
	}

	copyTo(folder, cb) {
		const src = this.getLocation();
		const dest = path.join(folder, path.basename(this.location));

		fs.access(dest, err => {
			if (err) {
				logger.log(`Copying ${this.getTitle()} in ${path.basename(folder)}.`);
				return fs.copy(src, dest, err => {
					if (err) {
						logger.log(`Error copying ${this.getTitle()}.`);
						return cb && cb(err);
					}

					logger.log(`Copied ${this.getTitle()}.`);
					cb && cb(err);
				});
			}

			logger.log(`${this.getTitle()} already exists.`);
			cb && cb();
		});
	}
}

class Tracklist {
	constructor(arr, name) {
		this.tracks = [];
		arr[0].track.forEach(obj => this.tracks.push(new Track(obj)));
	}

	getTracks() {
		return this.tracks;
	}

	getTrackCount() {
		return this.tracks.length;
	}

	copyAllTo(folder, cb) {
		let asyncCounter = this.getTrackCount();
		logger.log(`Copying ${this.getTrackCount()} tracks in ${path.basename(folder)}.`);

		const trackCallback = err => {
			asyncCounter--;
			if (asyncCounter === 0) {
				logger.log(`Copied ${this.getTrackCount()} tracks.`);

				return cb && cb(err);
			}

			this.getTracks()[asyncCounter - 1].copyTo(folder, trackCallback);
		};
		this.getTracks()[asyncCounter - 1].copyTo(folder, trackCallback);
	}

	copyAllToUnstable(folder, cb) {
		let asyncCounter = this.getTrackCount();
		logger.log(`Copying ${this.getTrackCount()} tracks in ${path.basename(folder)}.`);

		this.getTracks().forEach(track => track.copyTo(folder, err => {
			if (--asyncCounter === 0) {
				logger.log(`Copied ${this.getTrackCount()} tracks.`);

				return cb && cb(err);
			}
		}));
	}
}

class PlaylistFile {
	constructor(fileName, fileLocation) {
		this.fileName = fileName;
		this.fileLocation = fileLocation;
	}

	readFile(cb) {
		fs.readFile(this.fileLocation, 'utf8',(err, data) => cb(data));

		return this;
	}

	generateJSON(cb) {
		this.readFile(data =>
			xml2js.parseString(data, (err, json) => cb(json)));

		return this;
	}

	generateList(cb) {
		this.generateJSON(json =>
			cb(new Tracklist(json.playlist.trackList)));

		return this;
	}
}

class OutputFolder {
	constructor(name, fatherLoc, flag) {
		this.name = name;
		this.fatherLoc = fatherLoc;
		this.location = flag ? fatherLoc : path.join(this.fatherLoc, this.name);
	}

	create(cb) {
		fs.mkdirs(this.location, cb);
	}

	toString() {
		return this.location;
	}
}

let logger;
if (require.main === module) {
	const ARGS = process.argv.slice(2);
	logger = new Logger(true, 40);
	procedure(ARGS[0], path.basename(ARGS[0]), path.dirname(ARGS[0]), ARGS[1]);
} else {
    module.exports = (location, newFolFla) => {
    	logger = new Logger(true, 40);
		procedure(location, path.basename(location), path.dirname(location), newFolFla);
    }
}
